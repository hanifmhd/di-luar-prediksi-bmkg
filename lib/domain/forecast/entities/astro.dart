// coverage:ignore-file
import 'package:freezed_annotation/freezed_annotation.dart';

part 'astro.g.dart';

@JsonSerializable()
class Astro {
  @JsonKey(name: 'sunrise')
  String? sunrise;

  @JsonKey(name: 'sunset')
  String? sunset;

  @JsonKey(name: 'moonrise')
  String? moonrise;

  @JsonKey(name: 'moonset')
  String? moonset;

  Astro(
      {required this.sunrise,
      required this.sunset,
      required this.moonrise,
      required this.moonset});

  factory Astro.fromJson(Map<String, dynamic> json) => _$AstroFromJson(json);

  Map<String, dynamic> toJson() => _$AstroToJson(this);
}

// coverage:ignore-file
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecastday.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/current.dart';
import 'package:di_luar_prediksi_bmkg/domain/location/entities/location.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'forecast.g.dart';

@JsonSerializable()
class Forecast {
  @JsonKey(name: 'location')
  Location? location;

  @JsonKey(name: 'current')
  Current? current;

  @JsonKey(name: 'forecast')
  Forecastday? forecast;

  Forecast({this.location, this.current, this.forecast});

  factory Forecast.fromJson(Map<String, dynamic> json) =>
      _$ForecastFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND
// coverage:ignore-file
part of 'hour.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Hour _$HourFromJson(Map<String, dynamic> json) => Hour(
      time: json['time'] as String?,
      current: json['current'] == null
          ? null
          : Current.fromJson(json['current'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HourToJson(Hour instance) => <String, dynamic>{
      'time': instance.time,
      'current': instance.current,
    };

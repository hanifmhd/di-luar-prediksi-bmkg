// coverage:ignore-file
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/listforecastday.dart';

part 'forecastday.g.dart';

@JsonSerializable()
class Forecastday {
  @JsonKey(name: 'forecastday')
  List<ListForecastday>? forecastday;

  Forecastday({this.forecastday});

  factory Forecastday.fromJson(Map<String, dynamic> json) =>
      _$ForecastdayFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastdayToJson(this);
}

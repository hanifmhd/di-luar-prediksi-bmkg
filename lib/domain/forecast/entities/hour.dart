// coverage:ignore-file
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/current.dart';

part 'hour.g.dart';

@JsonSerializable()
class Hour {
  @JsonKey(name: 'time')
  String? time;

  @JsonKey(name: 'current')
  Current? current;

  Hour({
    this.time,
    this.current,
  });

  factory Hour.fromJson(Map<String, dynamic> json) => _$HourFromJson(json);

  Map<String, dynamic> toJson() => _$HourToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND
// coverage:ignore-file
part of 'forecastday.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Forecastday _$ForecastdayFromJson(Map<String, dynamic> json) => Forecastday(
      forecastday: (json['forecastday'] as List<dynamic>?)
          ?.map((e) => ListForecastday.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ForecastdayToJson(Forecastday instance) =>
    <String, dynamic>{
      'forecastday': instance.forecastday,
    };

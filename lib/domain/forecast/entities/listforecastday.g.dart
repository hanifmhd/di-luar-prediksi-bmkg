// GENERATED CODE - DO NOT MODIFY BY HAND
// coverage:ignore-file
part of 'listforecastday.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListForecastday _$ListForecastdayFromJson(Map<String, dynamic> json) =>
    ListForecastday(
      date: json['date'] as String?,
      astro: json['astro'] == null
          ? null
          : Astro.fromJson(json['astro'] as Map<String, dynamic>),
      hour: (json['hour'] as List<dynamic>?)
          ?.map((e) => Current.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ListForecastdayToJson(ListForecastday instance) =>
    <String, dynamic>{
      'date': instance.date,
      'astro': instance.astro,
      'hour': instance.hour,
    };

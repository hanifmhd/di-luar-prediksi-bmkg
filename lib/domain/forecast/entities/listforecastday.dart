// coverage:ignore-file
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/current.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/astro.dart';

part 'listforecastday.g.dart';

@JsonSerializable()
class ListForecastday {
  @JsonKey(name: 'date')
  String? date;

  @JsonKey(name: 'astro')
  Astro? astro;

  @JsonKey(name: 'hour')
  List<Current>? hour;

  ListForecastday({this.date, this.astro, this.hour});

  factory ListForecastday.fromJson(Map<String, dynamic> json) =>
      _$ListForecastdayFromJson(json);

  Map<String, dynamic> toJson() => _$ListForecastdayToJson(this);
}

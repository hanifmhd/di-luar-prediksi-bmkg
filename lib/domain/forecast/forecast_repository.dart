// coverage:ignore-file
import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_params.dart';
import 'package:rxdart/utils.dart';

abstract class ForecastRepository {
  const ForecastRepository();

  Future<Either<ErrorAndStackTrace, Forecast?>> getWeatherByLocation(
      GetWeatherByLocationParams getWeatherByLocationParams);
}

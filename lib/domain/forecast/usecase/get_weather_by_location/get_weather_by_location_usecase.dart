// coverage:ignore-file
import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/forecast_repository.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_params.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/utils.dart';

@injectable
class GetWeatherByLocationUsecase {
  const GetWeatherByLocationUsecase(this.repository);

  final ForecastRepository repository;

  Future<Either<ErrorAndStackTrace, Object?>> call(
      [GetWeatherByLocationParams? args]) {
    if (args == null) throw Exception('Arguments required');
    return repository.getWeatherByLocation(args);
  }
}

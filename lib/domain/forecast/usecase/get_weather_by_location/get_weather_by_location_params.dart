// coverage:ignore-file
import 'package:freezed_annotation/freezed_annotation.dart';

part 'get_weather_by_location_params.freezed.dart';
part 'get_weather_by_location_params.g.dart';

@freezed
class GetWeatherByLocationParams with _$GetWeatherByLocationParams {
  const factory GetWeatherByLocationParams({
    required String location,
  }) = _GetWeatherByLocationParams;
  factory GetWeatherByLocationParams.fromJson(Map<String, dynamic> json) =>
      _$GetWeatherByLocationParamsFromJson(json);
}
// GENERATED CODE - DO NOT MODIFY BY HAND
// coverage:ignore-file
part of 'get_weather_by_location_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$GetWeatherByLocationParamsImpl _$$GetWeatherByLocationParamsImplFromJson(
        Map<String, dynamic> json) =>
    _$GetWeatherByLocationParamsImpl(
      location: json['location'] as String,
    );

Map<String, dynamic> _$$GetWeatherByLocationParamsImplToJson(
        _$GetWeatherByLocationParamsImpl instance) =>
    <String, dynamic>{
      'location': instance.location,
    };

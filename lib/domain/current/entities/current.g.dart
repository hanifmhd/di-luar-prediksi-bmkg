// GENERATED CODE - DO NOT MODIFY BY HAND
// coverage:ignore-file
part of 'current.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Current _$CurrentFromJson(Map<String, dynamic> json) => Current(
      last_updated: json['last_updated'] as String?,
      temp_c: (json['temp_c'] as num?)?.toDouble(),
      is_day: json['is_day'] as int?,
      condition: json['condition'] == null
          ? null
          : Condition.fromJson(json['condition'] as Map<String, dynamic>),
      wind_dir: json['wind_dir'] as String?,
      humidity: json['humidity'] as int?,
      uv: (json['uv'] as num?)?.toDouble(),
      time: json['time'] as String?,
    );

Map<String, dynamic> _$CurrentToJson(Current instance) => <String, dynamic>{
      'last_updated': instance.last_updated,
      'temp_c': instance.temp_c,
      'is_day': instance.is_day,
      'condition': instance.condition,
      'wind_dir': instance.wind_dir,
      'humidity': instance.humidity,
      'uv': instance.uv,
      'time': instance.time,
    };

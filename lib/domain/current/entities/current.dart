// coverage:ignore-file
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/condition.dart';

part 'current.g.dart';

@JsonSerializable()
class Current {
  @JsonKey(name: 'last_updated')
  String? last_updated;

  @JsonKey(name: 'temp_c')
  double? temp_c;

  @JsonKey(name: 'is_day')
  int? is_day;

  @JsonKey(name: 'condition')
  Condition? condition;

  @JsonKey(name: 'wind_dir')
  String? wind_dir;

  @JsonKey(name: 'humidity')
  int? humidity;

  @JsonKey(name: 'uv')
  double? uv;

  @JsonKey(name: 'time')
  String? time;

  Current(
      {this.last_updated,
      this.temp_c,
      this.is_day,
      this.condition,
      this.wind_dir,
      this.humidity,
      this.uv,
      this.time});

  factory Current.fromJson(Map<String, dynamic> json) =>
      _$CurrentFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentToJson(this);
}

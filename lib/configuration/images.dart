import 'package:flutter/material.dart';

const String _imagePath = 'assets/images';

class _Image extends AssetImage {
  const _Image(String fileName) : super('$_imagePath/$fileName');
}

class AppImages {
  static const weatherLoader = _Image('weather-loader.gif');
  static const day = _Image('day.png');
  static const night = _Image('night.png');

  static Future precacheAssets(BuildContext context) async {
    await precacheImage(weatherLoader, context);
    await precacheImage(day, context);
    await precacheImage(night, context);
  }
}


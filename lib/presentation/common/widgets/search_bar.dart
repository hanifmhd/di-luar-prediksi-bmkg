import 'package:di_luar_prediksi_bmkg/configuration/colors.dart';
import 'package:di_luar_prediksi_bmkg/configuration/fonts.dart';
import 'package:di_luar_prediksi_bmkg/presentation/common/widgets/spacer.dart';
import 'package:flutter/material.dart';

class WeatherSearchBar extends StatelessWidget {
  final String? hintText;
  final TextEditingController? controller;
  final void Function()? onClear;

  const WeatherSearchBar(
      {super.key,
      this.margin = const EdgeInsets.symmetric(horizontal: 24),
      this.hintText,
      this.controller,
      this.onClear});

  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
      margin: margin,
      decoration: ShapeDecoration(
        shape: const StadiumBorder(),
        color: Theme.of(context).scaffoldBackgroundColor,
        shadows: [
          BoxShadow(
            color: AppColors.black.withOpacity(0.4),
            blurRadius: 15,
            offset: const Offset(0, 2),
          )
        ],
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Icon(Icons.search, size: 24),
          const HSpacer(13),
          Expanded(
            child: TextFormField(
              autocorrect: false,
              enableSuggestions: false,
              controller: controller,
              decoration: InputDecoration(
                isDense: true,
                hintText: hintText,
                contentPadding: EdgeInsets.zero,
                hintStyle: const TextStyle(fontFamily: AppFonts.nunito),
                border: InputBorder.none,
              ),
            ),
          ),
          controller!.text.isNotEmpty
              ? GestureDetector(
                  onTap: onClear,
                  child: const Icon(
                    Icons.close,
                    color: AppColors.whiteGrey,
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}

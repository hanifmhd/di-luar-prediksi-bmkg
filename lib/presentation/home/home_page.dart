import 'dart:async';
import 'package:di_luar_prediksi_bmkg/configuration/colors.dart';
import 'package:di_luar_prediksi_bmkg/configuration/images.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/current.dart';
import 'package:di_luar_prediksi_bmkg/presentation/common/widgets/search_bar.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_bloc.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_event.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_state.dart';
import 'package:di_luar_prediksi_bmkg/utility/di.dart';
import 'package:di_luar_prediksi_bmkg/utility/globals.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _searchController = TextEditingController();
  late GetWeatherByLocationBloc _getWeatherByLocationBloc;
  Timer? _debounceSearchTimer;
  late ImageProvider<Object> _backgroundImage = AppImages.night;
  late int _selectedIndexForecast = 0;
  late Current _selectedForecastDetail =
      Current(temp_c: 0, humidity: 0, uv: 0, wind_dir: 'N');

  @override
  void initState() {
    _getWeatherByLocationBloc = getIt();
    _getWeatherByLocationBloc.add(GetWeatherByLocationInit());
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _searchController.text = 'Jakarta';
      _getWeatherByLocationBloc
          .add(GetWeatherByLocation(_searchController.text));
      _getWeatherByLocationBloc.stream
          .listen(_getWeatherByLocationBlocListener);
      _searchController.addListener(_onSearch);
    });
    super.initState();
  }

  void _onSearch() async {
    if (_debounceSearchTimer?.isActive ?? false) _debounceSearchTimer?.cancel();
    _debounceSearchTimer =
        Timer(Duration(milliseconds: Globals.debounceMillis), () async {
      var text = _searchController.text;
      if (text.isNotEmpty) {
        _getWeatherByLocationBloc.add(
          GetWeatherByLocation(text),
        );
      } else {
        setState(() {
          _searchController.clear();
        });
      }
    });
  }

  void _getWeatherByLocationBlocListener(state) {
    if (state is GetWeatherByLocationLoaded) {
      setState(() {
        _selectedForecastDetail = state.data.forecast!.forecastday![0].hour![0];
      });
      if (state.data.current!.is_day == 1) {
        setState(() {
          _backgroundImage = AppImages.day;
        });
      } else {
        setState(() {
          _backgroundImage = AppImages.night;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints.expand(),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: _backgroundImage,
          fit: BoxFit.cover,
          opacity: 0.5,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          padding: const EdgeInsets.only(
              top: Globals.mainAppbarPadding, right: 24, left: 24),
          child: Column(
            children: [
              WeatherSearchBar(
                hintText: 'Search location',
                controller: _searchController,
                onClear: () => _searchController.clear(),
              ),
              Expanded(
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: [
                    BlocBuilder<GetWeatherByLocationBloc,
                        GetWeatherByLocationState>(
                      bloc: _getWeatherByLocationBloc,
                      builder: (context, state) {
                        if (state is GetWeatherByLocationLoading) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        if (state is GetWeatherByLocationLoaded) {
                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          DateFormat('dd MMMM yyyy').format(
                                            DateTime.parse(
                                              state.data.current!.last_updated!,
                                            ).toLocal(),
                                          ),
                                        ),
                                        Text(
                                          '${state.data.location!.name!},',
                                          style: const TextStyle(
                                            fontSize: 24,
                                          ),
                                        ),
                                        Text(
                                          '${state.data.location!.region!},',
                                          style: const TextStyle(
                                            fontSize: 24,
                                          ),
                                        ),
                                        Text(
                                          state.data.location!.country!,
                                          style: const TextStyle(
                                            fontSize: 24,
                                          ),
                                        ),
                                        Text(
                                          '${state.data.current!.temp_c.toString()}° C',
                                          style: const TextStyle(
                                            fontSize: 24,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          state.data.current!.condition!.text!,
                                          style: const TextStyle(fontSize: 24),
                                        ),
                                        Image(
                                          image: NetworkImage(
                                            'https:${state.data.current!.condition!.icon!}',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 24.0),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      const Text('Sunrise'),
                                      Text(state.data.forecast!.forecastday![0]
                                          .astro!.sunrise!),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text('Sunset'),
                                      Text(state.data.forecast!.forecastday![0]
                                          .astro!.sunset!),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text('Moonrise'),
                                      Text(state.data.forecast!.forecastday![0]
                                          .astro!.moonrise!),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text('Moonset'),
                                      Text(state.data.forecast!.forecastday![0]
                                          .astro!.moonset!),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 32,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: state.data.current!.is_day == 1
                                      ? AppColors.whiteGrey.withOpacity(0.2)
                                      : AppColors.black.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                padding: const EdgeInsets.all(12),
                                child: Column(
                                  children: [
                                    const Text(
                                      'Today Forecast',
                                      textAlign: TextAlign.left,
                                    ),
                                    const SizedBox(
                                      height: 12,
                                    ),
                                    SizedBox(
                                      height: 120,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: state.data.forecast!
                                            .forecastday![0].hour!.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            margin: const EdgeInsets.only(
                                                right: 12),
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  _selectedIndexForecast =
                                                      index;
                                                  _selectedForecastDetail =
                                                      state
                                                          .data
                                                          .forecast!
                                                          .forecastday![0]
                                                          .hour![index];
                                                });
                                              },
                                              child: Column(
                                                children: [
                                                  Text(
                                                    '${state.data.forecast!.forecastday![0].hour![index].temp_c.toString()}° C',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          _selectedIndexForecast ==
                                                                  index
                                                              ? FontWeight.bold
                                                              : FontWeight
                                                                  .normal,
                                                    ),
                                                  ),
                                                  Image(
                                                    image: NetworkImage(
                                                      'https:${state.data.forecast!.forecastday![0].hour![index].condition!.icon}',
                                                    ),
                                                    opacity: _selectedIndexForecast ==
                                                            index
                                                        ? const AlwaysStoppedAnimation(
                                                            1)
                                                        : const AlwaysStoppedAnimation(
                                                            0.7,
                                                          ),
                                                  ),
                                                  Text(
                                                    DateFormat('Haaa').format(
                                                      DateTime.parse(
                                                        state
                                                            .data
                                                            .forecast!
                                                            .forecastday![0]
                                                            .hour![index]
                                                            .time
                                                            .toString(),
                                                      ).toLocal(),
                                                    ),
                                                    style: TextStyle(
                                                      fontWeight:
                                                          _selectedIndexForecast ==
                                                                  index
                                                              ? FontWeight.bold
                                                              : FontWeight
                                                                  .normal,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: state.data.current!.is_day == 1
                                      ? AppColors.whiteGrey.withOpacity(0.2)
                                      : AppColors.black.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                padding: const EdgeInsets.all(12),
                                child: Column(
                                  children: [
                                    Text(
                                        'Condition at ${DateFormat('Haaa').format(
                                      DateTime.parse(
                                        state.data.forecast!.forecastday![0]
                                            .hour![_selectedIndexForecast].time
                                            .toString(),
                                      ),
                                    )}'),
                                    const SizedBox(
                                      height: 12,
                                    ),
                                    Table(
                                      border: TableBorder.all(
                                        color: AppColors.whiteGrey,
                                        style: BorderStyle.solid,
                                        width: 1,
                                      ),
                                      children: [
                                        TableRow(children: [
                                          const Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Text(
                                              'Temperature',
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              '${_selectedForecastDetail.temp_c.toString()}° C',
                                            ),
                                          ),
                                        ]),
                                        TableRow(children: [
                                          const Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Text(
                                              'Wind Dir',
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              _selectedForecastDetail.wind_dir!,
                                            ),
                                          ),
                                        ]),
                                        TableRow(
                                          children: [
                                            const Padding(
                                              padding: EdgeInsets.all(8.0),
                                              child: Text(
                                                'Humidity',
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                _selectedForecastDetail
                                                    .humidity!
                                                    .toString(),
                                              ),
                                            ),
                                          ],
                                        ),
                                        TableRow(
                                          children: [
                                            const Padding(
                                              padding: EdgeInsets.all(8.0),
                                              child: Text(
                                                'UV',
                                                style: TextStyle(
                                                  fontSize: 16.0,
                                                  letterSpacing: 1.2,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                _selectedForecastDetail.uv!
                                                    .toString(),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        }
                        if (state is GetWeatherByLocationError &&
                            _searchController.text.isNotEmpty) {
                          return const Center(
                            child: Text('No location found'),
                          );
                        }
                        if (_searchController.text.isEmpty) {
                          return const Center(
                            child: Text('Search anything you want'),
                          );
                        }
                        return Container();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

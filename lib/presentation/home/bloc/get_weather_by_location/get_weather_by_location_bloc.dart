// coverage:ignore-file
import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/current.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_params.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_usecase.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_event.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_state.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class GetWeatherByLocationBloc
    extends Bloc<GetWeatherByLocationEvent, GetWeatherByLocationState> {
  final GetWeatherByLocationUsecase getWeatherByLocationUsecase;

  GetWeatherByLocationBloc(this.getWeatherByLocationUsecase)
      : super(GetWeatherByLocationInitializing()) {
    on<GetWeatherByLocationInit>((event, emit) {
      emit(GetWeatherByLocationInitializing());
    });

    on<GetWeatherByLocation>((event, emit) async {
      emit(GetWeatherByLocationLoading());
      await getData(emit, event.location);
    });
  }

  Future getData(emit, String location) async {
    try {
      var getWeatherByLocationParams = GetWeatherByLocationParams(
        location: location,
      );
      final result =
          await getWeatherByLocationUsecase.call(getWeatherByLocationParams);
      if (result.isLeft()) throw (result as Left).value;

      Forecast? data = (result as Right).value;
      if (data != null) {
        List<Current>? hour = data.forecast!.forecastday![0].hour;
        List<Current> newHour = [];
        for (var i = 0; i < hour!.length; i++) {
          DateTime now = DateTime.parse(
              DateFormat('yyyy-MM-dd hh:mm').format(DateTime.parse(
            DateTime.now().toString(),
          )));
          DateTime forecastTime = DateTime.parse(hour[i].time.toString());
          if (forecastTime.isAfter(now)) {
            newHour.add(hour[i]);
          }
        }
        if (newHour.isEmpty) {
          hour = data.forecast!.forecastday![1].hour;
          for (var i = 0; i < hour!.length; i++) {
            DateTime now = DateTime.parse(
                DateFormat('yyyy-MM-dd hh:mm').format(DateTime.parse(
              DateTime.now().toString(),
            )));
            DateTime forecastTime = DateTime.parse(hour[i].time.toString());
            if (forecastTime.isAfter(now)) {
              newHour.add(hour[i]);
            }
          }
        }
        data.forecast!.forecastday![0].hour = newHour;
        emit(GetWeatherByLocationLoaded(data));
      } else {
        emit(GetWeatherByLocationEmpty());
      }
    } catch (error, stackTrace) {
      if (kDebugMode) print(stackTrace);
      emit(
        GetWeatherByLocationError(
          error is ErrorAndStackTrace
              ? error.error.toString()
              : error.toString(),
        ),
      );
    }
  }
}

// coverage:ignore-file
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';
import 'package:equatable/equatable.dart';

abstract class GetWeatherByLocationState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetWeatherByLocationInitializing extends GetWeatherByLocationState {}

class GetWeatherByLocationLoading extends GetWeatherByLocationState {}

class GetWeatherByLocationEmpty extends GetWeatherByLocationState {}

class GetWeatherByLocationLoaded extends GetWeatherByLocationState {
  final Forecast data;

  GetWeatherByLocationLoaded(this.data);

  @override
  List<Object> get props => [data];
}

class GetWeatherByLocationError extends GetWeatherByLocationState {
  final String message;

  GetWeatherByLocationError(this.message);

  @override
  List<Object> get props => [message];
}

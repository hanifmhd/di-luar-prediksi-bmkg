// coverage:ignore-file
import 'package:equatable/equatable.dart';

abstract class GetWeatherByLocationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetWeatherByLocationInit extends GetWeatherByLocationEvent {}

class GetWeatherByLocation extends GetWeatherByLocationEvent {
  final String location;
  GetWeatherByLocation(this.location);

  @override
  List<Object> get props => [location];
}
import 'package:di_luar_prediksi_bmkg/app.dart';
import 'package:di_luar_prediksi_bmkg/configuration/theme_cubit.dart';
import 'package:di_luar_prediksi_bmkg/utility/di.dart';
import 'package:di_luar_prediksi_bmkg/utility/injectable_env.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await configureDependencies(environment: InjectableEnv.real);

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<ThemeCubit>(
          create: (context) => ThemeCubit(),
        ),
      ],
      child: const WeatherApp(),
    ),
  );
}

import 'package:di_luar_prediksi_bmkg/presentation/common/widgets/fade_route.dart';
import 'package:di_luar_prediksi_bmkg/presentation/splash/splash_page.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/home_page.dart';
import 'package:flutter/material.dart';

enum Routes { splash, home }

class _Paths {
  static const String splash = '/';
  static const String home = '/home';

  static const Map<Routes, String> _pathMap = {
    Routes.splash: _Paths.splash,
    Routes.home: _Paths.home,
  };

  static String of(Routes route) => _pathMap[route] ?? splash;
}

class AppNavigator {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case _Paths.home:
        return FadeRoute(
          page: const HomePage(),
        );
      case _Paths.splash:
        return FadeRoute(
          page: const SplashPage(),
        );
      default:
        return FadeRoute(
          page: const SplashPage(),
        );
    }
  }

  static Future? push<T>(Routes route, [T? arguments]) =>
      state?.pushNamed(_Paths.of(route), arguments: arguments);

  static Future? replaceWith<T>(Routes route, [T? arguments]) =>
      state?.pushReplacementNamed(_Paths.of(route), arguments: arguments);

  static void pop() => state?.pop();

  static NavigatorState? get state => navigatorKey.currentState;
}

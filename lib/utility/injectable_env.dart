class InjectableEnv {
  // don;t include hypens
  static const integrationTest = 'integrationTest';
  static const real = 'real';

  static const values = [
    real,
    integrationTest,
  ];
}

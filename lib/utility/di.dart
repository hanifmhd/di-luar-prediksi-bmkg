import 'dart:async';

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'di.config.dart';

final GetIt getIt = GetIt.I;

@InjectableInit()
Future<GetIt> configureDependencies({
  String? environment,
  EnvironmentFilter? environmentFilter,
}) async =>
    getIt.init(
      environment: environment,
      environmentFilter: environmentFilter,
    );

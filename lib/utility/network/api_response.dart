// coverage:ignore-file
import 'package:di_luar_prediksi_bmkg/domain/common/entities/meta.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';

class ApiResponse<T, K> {
  final T? results;
  ApiResponse({this.results});
  factory ApiResponse.fromJson(Map<String, dynamic>? json) {
    if (json == null) throw Exception('Terjadi Kendala');
    return _$ApiResponseFromJson(json);
  }
}

ApiResponse<T, K> _$ApiResponseFromJson<T, K>(Map<String, dynamic> json) {
  return ApiResponse<T, K>(
    results: Generic.fromJson(json),
  );
}

class ApiResponseError<T, K> {
  final Meta? error;
  ApiResponseError({this.error});
  factory ApiResponseError.fromJson(Map<String, dynamic>? json) {
    if (json == null) throw Exception('Terjadi Kendala');
    return _$ApiResponseErrorFromJson(json);
  }
}

ApiResponseError<T, K> _$ApiResponseErrorFromJson<T, K>(Map<String, dynamic> json) {
  return ApiResponseError<T, K>(
    error: Meta.fromJson(json['error']),
  );
}

class Generic {
  /// If T is a List, K is the subtype of the list.
  static T? fromJson<T, K>(dynamic json) {
    if (json is Iterable) {
      return _fromJsonList<K>(json as List) as T;
    } else if (T == Forecast) {
      return Forecast.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Object) {
      return json;
    } else {
      return null;
    }
  }

  static List<K>? _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList.map<K>((dynamic json) {
      final item = fromJson<K, void>(json);
      if (item == null) throw Exception('unknown class');
      return item;
    }).toList();
  }
}

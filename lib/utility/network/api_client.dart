// coverage:ignore-file
import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/dart_define.gen.dart';
import 'package:di_luar_prediksi_bmkg/stub/dio/dio_adapter_mobile.dart';
import 'package:di_luar_prediksi_bmkg/utility/network/api_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:rxdart/utils.dart';

@injectable
class ApiClient with DioMixin implements Dio {
  ApiClient() {
    interceptors.add(
      QueuedInterceptorsWrapper(
        onRequest: (options, handler) async {
          Logger().i(options.data);
          return handler.next(options);
        },
        onResponse: (resp, handler) {
          if (kDebugMode) {
            final isResponseTypeBytes =
                resp.requestOptions.responseType == ResponseType.bytes;
            Logger().i({
              'api':
                  '${resp.statusCode}: ${resp.requestOptions.baseUrl}${resp.requestOptions.path}',
              'headers': resp.requestOptions.headers,
              'queryParams': resp.requestOptions.queryParameters,
              'body': resp.requestOptions.data,
              'response': isResponseTypeBytes ? null : resp.data,
            });
          }
          return handler.next(resp);
        },
        onError: (err, handler) async {
          if (kDebugMode) {
            Logger().i({
              'api':
                  '${err.response?.statusCode ?? 0}: ${err.requestOptions.baseUrl}${err.requestOptions.path}',
              'headers': err.requestOptions.headers,
              'queryParams': err.requestOptions.queryParameters,
              'body': err.requestOptions.data,
              'response': err.response?.data
            });
          }
          return handler.next(err);
        },
      ),
    );
  }

  @override
  BaseOptions get options => BaseOptions(
        baseUrl: DartDefine.apiUrl,
        connectTimeout: const Duration(milliseconds: 25000),
        receiveTimeout: const Duration(milliseconds: 15000),
        contentType: 'application/json',
      );

  @override
  HttpClientAdapter get httpClientAdapter => getAdapter();

  Future<Either<ErrorAndStackTrace, T?>> apiGet<T, K>(
    String path, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final response = await get(
        path,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      var apiResponse = ApiResponse<T, K>.fromJson(response.data);
      return right(apiResponse.results);
    } catch (error, stackTrace) {
      if (error is DioException) {
        var apiResponse =
            ApiResponse<void, void>.fromJson(error.response?.data);
        return left(ErrorAndStackTrace(
          apiResponse.toString(),
          stackTrace,
        ));
      }
      return left(ErrorAndStackTrace(error, stackTrace));
    }
  }
}

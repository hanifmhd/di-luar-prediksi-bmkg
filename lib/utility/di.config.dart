// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:di_luar_prediksi_bmkg/data/forecast/forecast_repository_implementation.dart'
    as _i6;
import 'package:di_luar_prediksi_bmkg/data/forecast/forecast_service.dart'
    as _i4;
import 'package:di_luar_prediksi_bmkg/data/forecast/mock_forecast_repository.dart'
    as _i7;
import 'package:di_luar_prediksi_bmkg/domain/forecast/forecast_repository.dart'
    as _i5;
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_usecase.dart'
    as _i8;
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_bloc.dart'
    as _i9;
import 'package:di_luar_prediksi_bmkg/utility/network/api_client.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

const String _real = 'real';
const String _integrationTest = 'integrationTest';

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.ApiClient>(() => _i3.ApiClient());
    gh.factory<_i4.ForecastService>(() => _i4.ForecastService());
    gh.factory<_i5.ForecastRepository>(
      () => _i6.ForecastRepositoryImplementation(gh<_i4.ForecastService>()),
      registerFor: {_real},
    );
    gh.singleton<_i5.ForecastRepository>(
      _i7.StubMockForecastRepository(gh<_i4.ForecastService>()),
      registerFor: {_integrationTest},
    );
    gh.factory<_i8.GetWeatherByLocationUsecase>(
        () => _i8.GetWeatherByLocationUsecase(gh<_i5.ForecastRepository>()));
    gh.factory<_i9.GetWeatherByLocationBloc>(() =>
        _i9.GetWeatherByLocationBloc(gh<_i8.GetWeatherByLocationUsecase>()));
    return this;
  }
}

import 'package:di_luar_prediksi_bmkg/data/forecast/forecast_service.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/forecast_repository.dart';
import 'package:di_luar_prediksi_bmkg/utility/injectable_env.dart';
import 'package:injectable/injectable.dart';
import 'package:mockito/annotations.dart';

import 'mock_forecast_repository.mocks.dart';

@GenerateMocks(<Type>[ForecastRepository])
@Singleton(as: ForecastRepository, env: [InjectableEnv.integrationTest])
class StubMockForecastRepository extends MockForecastRepository
    implements ForecastRepository {
  final ForecastService service;
   StubMockForecastRepository(this.service);
}

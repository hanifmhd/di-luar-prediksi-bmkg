// coverage:ignore-file
import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/data/forecast/forecast_service.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/forecast_repository.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_params.dart';
import 'package:di_luar_prediksi_bmkg/utility/injectable_env.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/utils.dart';

@Injectable(as: ForecastRepository, env: [InjectableEnv.real])
class ForecastRepositoryImplementation extends ForecastRepository {
  const ForecastRepositoryImplementation(this.service);

  final ForecastService service;

  @override
  Future<Either<ErrorAndStackTrace, Forecast?>> getWeatherByLocation(
      GetWeatherByLocationParams getWeatherByLocationParams) async {
    return await service.getWeatherByLocation(getWeatherByLocationParams);
  }
}

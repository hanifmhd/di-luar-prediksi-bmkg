// coverage:ignore-file
import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/dart_define.gen.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_params.dart';
import 'package:di_luar_prediksi_bmkg/utility/di.dart';
import 'package:di_luar_prediksi_bmkg/utility/network/api_client.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/utils.dart';

@injectable
class ForecastService {
  static var path = {
    'getForecast': '/forecast.json',
  };

  ApiClient get client => getIt();

  Future<Either<ErrorAndStackTrace, Forecast?>> getWeatherByLocation(
      GetWeatherByLocationParams getWeatherByLocationParams) async {
    return await client.apiGet<Forecast, Map<String, dynamic>>(
      '${path['getForecast']!}?key=${DartDefine.apiKey}&q=${getWeatherByLocationParams.location}&days=2',
    );
  }
}
import 'package:dartz/dartz.dart';
import 'package:rxdart/rxdart.dart';

abstract class UseCase<T, A> {
  Future<Either<ErrorAndStackTrace, T?>> call([A args]);
}
import 'package:di_luar_prediksi_bmkg/configuration/theme_cubit.dart';
import 'package:di_luar_prediksi_bmkg/configuration/themes.dart';
import 'package:di_luar_prediksi_bmkg/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherApp extends StatelessWidget {
  const WeatherApp({super.key});

  @override
  Widget build(BuildContext context) {
    var themeCubit = BlocProvider.of<ThemeCubit>(context, listen: true);
    var isDark = themeCubit.isDark;

    return MaterialApp(
      color: Colors.white,
      debugShowCheckedModeBanner: false,
      title: 'DLPB',
      theme: isDark ? Themes.darkTheme : Themes.lightTheme,
      navigatorKey: AppNavigator.navigatorKey,
      onGenerateRoute: AppNavigator.onGenerateRoute,
    );
  }
}

import 'package:di_luar_prediksi_bmkg/data/forecast/forecast_repository_implementation.dart';
import 'package:di_luar_prediksi_bmkg/data/forecast/forecast_service.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/forecast_repository.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_usecase.dart';
import 'package:di_luar_prediksi_bmkg/presentation/home/bloc/get_weather_by_location/get_weather_by_location_bloc.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void setupLocator() {
  // bloc
  locator.registerFactory(() => GetWeatherByLocationBloc(locator()));

  // usecase
  locator.registerLazySingleton(() => GetWeatherByLocationUsecase(locator()));

  // repository
  locator.registerLazySingleton<ForecastRepository>(
    () => ForecastRepositoryImplementation(locator()),
  );

  // external
  locator.registerLazySingleton(() => ForecastService());
}

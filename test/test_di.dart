import 'package:get_it/get_it.dart';
import 'package:di_luar_prediksi_bmkg/utility/di.dart';
import 'package:di_luar_prediksi_bmkg/utility/injectable_env.dart';

Future<GetIt> configureTestDependencies() async =>
    configureDependencies(environment: InjectableEnv.integrationTest);

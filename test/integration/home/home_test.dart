import 'package:dartz/dartz.dart';
import 'package:di_luar_prediksi_bmkg/data/forecast/mock_forecast_repository.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/condition.dart';
import 'package:di_luar_prediksi_bmkg/domain/current/entities/current.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/astro.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecast.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/forecastday.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/entities/listforecastday.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/forecast_repository.dart';
import 'package:di_luar_prediksi_bmkg/domain/forecast/usecase/get_weather_by_location/get_weather_by_location_params.dart';
import 'package:di_luar_prediksi_bmkg/domain/location/entities/location.dart';
import 'package:di_luar_prediksi_bmkg/utility/di.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';

import '../../test_di.dart';

void main() {
  setUp(configureTestDependencies);
  tearDown(getIt.reset);

  void setupSearchWeatherByLocationSuccess() {
    final mockForecastRepository =
        getIt<ForecastRepository>() as StubMockForecastRepository;
    when(mockForecastRepository.getWeatherByLocation(
            const GetWeatherByLocationParams(location: 'sylveon')))
        .thenAnswer(
      (_) async => Right<ErrorAndStackTrace, Forecast?>(
        Forecast(
          location: Location(
            name: 'Jakarta',
            region: 'Jakarta Raya',
            country: 'Indonesia',
            lat: -6.21,
            lon: 106.85,
          ),
          current: Current(
            is_day: 1,
            last_updated: '2023-11-21 23:00',
            temp_c: 26.0,
            humidity: 94,
            uv: 1.0,
            wind_dir: 'S',
            condition: Condition(
              text: 'Light rain',
              icon: '//cdn.weatherapi.com/weather/64x64/night/296.png',
              code: 1183,
            ),
          ),
          forecast: Forecastday(
            forecastday: [
              ListForecastday(
                date: '2023-11-21',
                astro: Astro(
                  sunrise: '05:26 AM',
                  sunset: '05:51 PM',
                  moonrise: '12:36 PM',
                  moonset: '12:21 AM',
                ),
                hour: [
                  Current(
                    is_day: 0,
                    time: '2023-11-21 00:00',
                    temp_c: 30.0,
                    humidity: 61,
                    uv: 1.0,
                    wind_dir: 'N',
                    condition: Condition(
                      text: 'Partly cloudy',
                      icon: '//cdn.weatherapi.com/weather/64x64/night/116.png',
                      code: 1003,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  testWidgets('Search weather by location', (WidgetTester tester) async {
    setupSearchWeatherByLocationSuccess();
  });
}

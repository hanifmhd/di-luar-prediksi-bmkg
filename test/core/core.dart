import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:di_luar_prediksi_bmkg/configuration/themes.dart';
import 'package:di_luar_prediksi_bmkg/routes.dart';

typedef PumperWidget = Widget Function(Widget widget);
typedef PumperRoute = Widget Function(String route);

Widget material(String? route) {
  return MaterialApp(
    color: Colors.white,
    debugShowCheckedModeBanner: false,
    title: 'DLPB',
    theme: Themes.darkTheme,
    navigatorKey: AppNavigator.navigatorKey,
    onGenerateRoute: AppNavigator.onGenerateRoute,
  );
}

bool findTextAndTap(InlineSpan visitor, String text) {
  if (visitor is TextSpan &&
      visitor.text == text &&
      visitor.recognizer != null &&
      visitor.recognizer is TapGestureRecognizer) {
    (visitor.recognizer as TapGestureRecognizer?)?.onTap?.call();

    return false;
  }

  return true;
}

bool tapTextSpan(RichText richText, String text) {
  final isTapped = !richText.text.visitChildren(
    (visitor) => findTextAndTap(visitor, text),
  );

  return isTapped;
}

void mockedCallback() {}

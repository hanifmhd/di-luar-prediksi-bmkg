# Di Luar Prediksi BMKG Assessment

## Requirements
### Windows
* **Operating Systems**: Windows 10 or later (64-bit), x86-64 based.
* **Disk Space**: 1.64 GB (does not include disk space for IDE/tools).
* **Tools**: Flutter depends on these tools being available in your environment.
  * [Windows PowerShell 5.0](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell) or newer (this is pre-installed with Windows 10)
  * [Git for Windows](https://git-scm.com/download/win) 2.x, with the **Use Git from the Windows Command Prompt** option. If Git for Windows is already installed, make sure you can run git commands from the command prompt or PowerShell.

More: https://docs.flutter.dev/get-started/install/windows

### MacOS
* **Operating Systems**: macOS
* **Disk Space**: 2.8 GB (does not include disk space for IDE/tools).
* **Tools**: Flutter uses `git` for installation and upgrade. We recommend installing [Xcode](https://developer.apple.com/xcode/), which includes git, but you can also [install git separately](https://git-scm.com/download/mac).

More: https://docs.flutter.dev/get-started/install/macos
## Supported Version
1. Minimum Dart/SDK Version: >=3.2.0 <4.0.0
2. Minimum Flutter version: 3.16.0
3. Platform: Desktop (MacOS/Windows), Web, Android & iOS
4. Android Studio version: 2022.3
5. Xcode version: 15.0.1

## APK
Firebase App Distribution: https://console.firebase.google.com/u/0/project/di-luar-prediksi-bmkg/overview

# Project Structure

## Architectural Pattern
This project implement Clean [Architecture by Fernando Cejas](https://github.com/android10/Android-CleanArchitecture)

### Clean architecture
![Image Clean architecture](/resources/clean_architecture.png)

### Architectural approach
![Image Architectural approach](/resources/clean_architecture_layers.png)

### Architectural reactive approach
![Image Architectural reactive approach](/resources/clean_architecture_layers_details.png)

### BLoC flow communication
![Image BLoC flow communication](/resources/bloc_flow.png)

## Usecases
[![Splash screen](/resources/0.gif)](/resources/0.mov)

Home | Day
:-------------------------: | :-------------------------: 
![Home page](/resources/1.png) | ![Day](/resources/2.png)

Night | Not Found
:-------------------------: | :-------------------------: 
![Night](/resources/3.png) | ![Not found](/resources/4.png)

## Module
For the high level hierarchy, the project separate into 3 main modules, which are :

1. Presentation

> This module represent the Presentation layer. It consists of (Business Logic) BLoC, widgets, etc. The classes are separated based on page, for examples : favorite, home, item, etc.

2. Domain

> This module represent the Domain layer. It consists of interactors/use cases and models and doesn’t know anything outside.

3. Data

> This module represent the Data layer. It consists of data sources: network call, mock data, disk data, and cache data.

## Dependencies
1. [cupertino_icons](https://pub.dev/packages/cupertino_icons): ^1.0.2
2. [flutter_bloc](https://pub.dev/packages/flutter_bloc): ^8.1.3
3. [dartz](https://pub.dev/packages/dartz): ^0.10.1
4. [rxdart](https://pub.dev/packages/rxdart): ^0.27.7
5. [freezed_annotation](https://pub.dev/packages/freezed_annotation): ^2.4.1
6. [json_annotation](https://pub.dev/packages/json_annotation): ^4.8.1
7. [dio](https://pub.dev/packages/dio): ^5.3.3
8. [injectable](https://pub.dev/packages/injectable): ^2.3.2
9. [logger](https://pub.dev/packages/logger): ^2.0.2+1
10. [get](https://pub.dev/packages/get): ^4.6.6
11. [get_it](https://pub.dev/packages/get_it): ^7.6.4
12. [equatable](equatable): ^2.0.5
13. [firebase_core](https://pub.dev/packages/firebase_core): ^2.22.0
14. [intl](https://pub.dev/packages/intl): ^0.18.1
15. [mockito](https://pub.dev/packages/mockito): ^5.4.3

# How to run project
Before running on your local, install that requirements first:
1. Clone this repo by running `git clone https://gitlab.com/hanifmhd/di-luar-prediksi-bmkg.git`
2. Install flutter from [here](https://docs.flutter.dev/get-started/install/macos)
3. Open project, then run `flutter doctor -v` to check minimum system requirements
4. If you want to run iOS don't forget to install Xcode by following [this](https://docs.flutter.dev/get-started/install/macos#install-xcode)
5. Clean and install dependencies to make sure the project is fresh `flutter pub clean && flutter pub get`
6. Generate global files by running `flutter pub run build_runner build --delete-conflicting-outputs`
7. Generate environment variable by running `flutter pub run dart_define generate --API_URL=$API_URL --API_KEY=$API_KEY`
8. Run project with value of environment variable by running ``flutter run --dart-define=API_URL=$API_URL --dart-define=API_KEY=$API_KEY``

Here are some useful flutter commands for executing this example:
* `flutter pub clean` - Clean dependencies for a Flutter package
* `flutter pub get` - Get dependencies for a Flutter package
* `flutter pub run build_runner build --delete-conflicting-outputs` - Build the entire project
* `flutter pub run build_runner watch --delete-conflicting-outputs` - Auto build when code changes
* `flutter test` - Execute unit test
* `flutter pub run dart_define generate --API_URL=$API_URL --API_KEY=$API_KEY` - Generate dart_define.json file with value of API_URL and API_KEY
* `flutter run --dart-define=API_URL=$API_URL --dart-define=API_KEY=$API_KEY` - Run project with value of API_URL and API_KEY
* `flutter build apk --dart-define-from-file=dart_define.json` - Build apk with value of API_URL and API_KEY
* `flutter build ipa --dart-define-from-file=dart_define.json` - Build ipa with value of API_URL and API_KEY

# Unit Test
Overall codes
[![Code coverage](/resources/code_coverage.png)](https://hanifmhd.gitlab.io/-/di-luar-prediksi-bmkg/-/jobs/5608374874/artifacts/coverage/index.html)

Only integration testing
![Integration Test](/resources/integration_test.png)

# Sequence Diagram
1. Search weather by location
> This sequence diagram is the interaction in Home page: `get weather by location`.

```mermaid
sequenceDiagram
    User->>+Mobile App: Open DLPB
    User->>+Mobile App: Typing location and search weather forecast by location
    Mobile App->>+Server: Get weather forecast by location (limit day: 2)
    Server-->>-Mobile App: Return list weather today forecast
    Mobile App-->>-User: Show list weather today forecast
    User->>+Mobile App: Click one of today forecast list
    Mobile App-->>-User: Show table of selected forecast (temperature, wind dir, humidity, UV)
```

